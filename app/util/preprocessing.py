class Preprocessor():

    # list of punctuations 
    PUNCTS = [',', ':', ')', '(', '-', '!', '?', '|', ';', "'", '$', '&', '[', ']', '>', '%', '=', '#', '*', '\\',  '~', '@', '£',
    '_', '{', '}', '©', '^', '®', '`',  '<', '→', '°', '€', '™', '›',  '♥', '←', '×', '§', '″', '′', 'Â', '█', '½', 'à', '…', 
    '“', '★', '”', '–', '●', 'â', '►', '−', '¢', '²', '¬', '░', '¶', '↑', '±', '¿', '▾', '═', '¦', '║', '―', '¥', '▓', '—', '‹', '─', 
    '▒', '：', '¼', '⊕', '▼', '▪', '†', '■', '’', '▀', '¨', '▄', '♫', '☆', 'é', '¯', '♦', '¤', '▲', 'è', '¸', '¾', 'Ã', '⋅', '‘', '∞', 
    '）', '↓', '、', '│', '（', '»', '，', '♪', '╩', '╚', '³', '・', '╦', '╣', '╔', '╗', '▬', '❤', 'ï', 'Ø', '¹', '≤', '‡', '√', ]

    # list of conjunctions
    CONJUNCTIONS = ['+', 'w/', 'with']


    def remove_non_breaking_space(self,sentence):
        """
        Removes non breaking space from the input string
        """
        return sentence.replace('\xc2\xa0\xc2\xa0', ' ')

    def remove_unicode_char(self,sentence):
        """
        Removes unicode characters from the input string
        """
        return sentence.encode('ascii', 'ignore').decode('ascii')


    def convert_to_lowercase(self,sentence):
        """
        Converts input string to lowercase'
        """
        return sentence.lower()

    def remove_special_char(self,sentence):
        for each_char in self.PUNCTS:
            sentence = sentence.replace(each_char, " ")
        return sentence

    def clean_sentence(self,sentence):
        """
        Cleans input string
        """
        if sentence != None and sentence != '' and sentence != 'NILL':
            sentence = self.remove_non_breaking_space(sentence)
            sentence = self.remove_unicode_char(sentence)
            sentence = self.convert_to_lowercase(sentence)
            sentence = self.remove_special_char(sentence)
            return sentence    
        else:
            return None
        
    def separate_with_substring(self,sentence):
        """
        Separates substring followed by few CONJUNCTIONS = {'with', 'w/', '+'} 
        from the sentence 
        

        Parameters:
        ----------
            sentence (str): name or title of the product

        Returns:
        --------
            shortened_sentence: original sentence after removing the substring after the 
                                conjunctions if present
            with_substring: the substring of the sentence after the conjunction keyword
        """

        #tokenize sentence
        sentence_tokens = sentence.split()

        # get the split index as the index of the conjunction
        if "with" in sentence_tokens:
            split_index = sentence_tokens.index("with")
        elif "w/" in sentence_tokens:
            split_index = sentence_tokens.index("w/")
        elif "+" in sentence_tokens:
            split_index = sentence_tokens.index("+")

        # between 'with' and '+' give preference to 'with'
        if "with" in sentence_tokens and "+" in sentence_tokens:
            with_index = sentence_tokens.index("with")
            plus_index = sentence_tokens.index("+")
            if with_index < plus_index:
                split_index = with_index
            else:
                split_index = plus_index
        # between 'w/' and '+' give preference to 'w/'
        if "w/" in sentence_tokens and "+" in sentence_tokens:
            with_index = sentence_tokens.index("w/")
            plus_index = sentence_tokens.index("+")
            if with_index < plus_index:
                split_index = with_index
            else:
                split_index = plus_index

        # join sentence tokens upto the split index to form the shortened tokens
        shortened_sentence = " ".join(sentence_tokens[0:split_index])
        # join sentence tokens from split index+1 to the end to form the with substring 
        with_substring = " ".join(sentence_tokens[(split_index + 1):])
        return shortened_sentence, with_substring

    def separate_for_substring(self,sentence):
        """
        Separates substring followed by for keyword from the sentence

        Parameters:
        ----------
            sentence (str): name or title of the product

        Returns:
        --------
            shortened_sentence: original sentence after removing the substring after the 
                                'for' keyword if present
            for_substring: the substring of the sentence after the 'for' keyword

        """
        # tokenize the sentence
        sentence_tokens = sentence.split()
        # get the split index as the index of 'for'
        split_index = sentence_tokens.index("for")
        # exclude cases where 'for' is followed by 'men' or 'women'
        if sentence_tokens[split_index + 1] != "men" and sentence_tokens[split_index + 1] != "women":
            # join sentence tokens upto the split index to get the shortened sentence
            shortened_sentence = " ".join(sentence_tokens[0:split_index])
            # join sentence tokens from split index+1 to the end to get the for substring
            for_substring = " ".join(sentence_tokens[(split_index + 1) :])
        else:
            shortened_sentence = sentence
            for_substring = None
        return shortened_sentence, for_substring
        

            
    def preprocess_input_sample(self,title):
        """
        Preprocesses the input string

        Parameters:
        ----------
            title (str): name or title of the product

        Returns:
        --------
            title_padded_vectors (list of lists): list of padded sequence vectors
        """
        if title != None:
            clean_title = self.clean_sentence(title)
            if ' with ' in clean_title or ' w/ ' in clean_title or ' + ' in clean_title:
                shortened_title, with_substring = self.separate_with_substring(clean_title)
            else:
                shortened_title = clean_title
                with_substring = None
            if ' for ' in shortened_title:
                processed_title, for_substring = self.separate_for_substring(shortened_title)
            else:
                processed_title = shortened_title
                for_substring = None        
            return processed_title
        else:
            return None
        