# import dependencies

from flair.data import Sentence
from flair.models import SequenceTagger
from flair.embeddings import WordEmbeddings, CharacterEmbeddings, FlairEmbeddings, StackedEmbeddings
from flair.data import TaggedCorpus
from flair.data_fetcher import NLPTaskDataFetcher, NLPTask
from flair.trainers import ModelTrainer
from flair.visual.training_curves import Plotter                             
import boto3
import os
from repositories.mlmodel import model

    
'Load the model architecture and weights'
class Inference():
    TAG_TYPE = 'ner'
    
    def predict_tags(self,test_sequence_X):
        """
        Predict tags for the test sequence

        Parameters:
        -----------
            test_sequence_X (string): test sequence 
            model (flair.models.sequence_tagger_model.SequenceTagger): loaded model


        Returns:
        --------
            sentence_dict (dict): sentence converted to dictionary predicted tags & entity spans
        """
        # create example sentence
        sentence = Sentence(test_sequence_X)
        # predict tags and print
        model.predict(sentence)
        sentence_dict = sentence.to_dict(tag_type=self.TAG_TYPE)
        return sentence_dict

    def extract_results(self,sentence_dict):
        """
        Extract brand, model and product from the sentence_dict

        Parameters:
        -----------
            sentence_dict (dict): sentence converted to dictionary predicted tags & entity spans

        Returns:
        --------
            attribute_dict (dict): dictionary containing predicted brand, model & product key value pairs
        """
        attribute_dict =   {'b': None,
                            'm': None,
                            'p': None,
                            'product_info':
                                {'for': None,
                                'color': None,
                                }
                            }

        for each_entity in sentence_dict['entities']:
            if each_entity['type'] == 'B':
                attribute_dict['b'] = each_entity['text']
            elif each_entity['type'] == 'M':
                attribute_dict['m'] = each_entity['text']
            else:
                attribute_dict['p'] = each_entity['text']
        return attribute_dict
        

