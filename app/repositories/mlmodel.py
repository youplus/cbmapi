from flair.models import SequenceTagger
import os

class MlModel():

    currentPath = os.path.dirname(os.path.realpath(__file__))
    parentPath = os.path.abspath(os.path.join(currentPath, os.pardir))
    ml_model_path = parentPath + '/MLModels/'

    def load_model(self):
        """
        Load the trained model

        Returns:
        --------
            model (flair.models.sequence_tagger_model.SequenceTagger): loaded model
        """
        path_to_model = self.ml_model_path + 'final-model.pt'
        model = SequenceTagger.load_from_file(path_to_model)
        return model

obj = MlModel()
model = obj.load_model()

