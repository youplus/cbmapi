from util.inference import Inference
from util.preprocessing import Preprocessor


class Product():
    """
    A class to represent a Product

    Attributes:
    -----------
        title (str): name or title of the product
        url (str): URL of the product
        bread_crumb (str): taxonomy of the product

    Methods:
    --------
        set_title(title=None): mutator method for title
        get_title(): accessor method for title
        get_product_attributes(): returns product attributes as key-value pairs

    """

    def __init__(self, title=None):
        """
        constructor for initializing attributes of class Product

        Parameters:
        -----------
            title (str): name or title of the product
        """
        self.title = title
    

    # accessor and mutator methods for attributes of class Product
    def set_title(self, title):
        self.title = title
    
    def get_title(self):
        return self.title 
    
    def get_product_attributes(self):
        """
        Predicts product attributes (brand, model, category, etc) from 
        product information

        Parameters:
        -----------
            product (object): instance of class product

        Returns:
        --------
            product_attributes (dict): dictionary containing product 
            attribute key-value pairs

        """
        try:
            obj = Inference()
            preprocessor = Preprocessor()
            # initialize the product attributes dictionary
            product_attributes =   {'b': None,
                                    'm': None,
                                    'p': None,
                                    'product_info':
                                        {'for': None,
                                        'color': None,
                                        }
                                    }
            
            if self.title != None and self.title != '':
                # preprocess input title
                preprocessed_title = preprocessor.preprocess_input_sample(self.title)
                # get predicted tags for title vector
                pred_tags_dict =obj.predict_tags(preprocessed_title)
                # get product attributes dictionary
                product_attributes = obj.extract_results(pred_tags_dict)
            return product_attributes  
            
        except Exception as e:
            print(e)

    def predict(self, title):
        """
        Predicts product attributes (brand, model, product etc) from 
        product title

        Parameters:
        -----------
            title (String) : Product title

        Returns:
        --------
            product_attributes (dict): dictionary containing product 
            attribute key-value pairs
        """


        try:
            result = self.get_product_attributes()
            product_attributes = {}
            product_attributes['Brand'] = result['b'] if result['b'] else ''
            product_attributes['Model'] = result['m'] if result['m'] else ''
            product_attributes['Product'] = result['p'] if result['p'] else ''
            return product_attributes
        except Exception as e:
            print (e)
            return None



    
