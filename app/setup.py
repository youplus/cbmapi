
import os
import boto3
import config

def download_model_from_s3():
    """
    Download pre-trained model from S3

    Returns:
    --------
        Status of the model, either getting downloaded or already present in model folder
    """
    currentPath = os.path.dirname(os.path.realpath(__file__))
    ml_model_path = currentPath + '/MLModels/'

    model_name = 'final-model.pt'
    if not os.path.isdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), ml_model_path)):
        os.makedirs(ml_model_path)
    if not os.path.isdir(os.path.join(ml_model_path, model_name)):
      session = boto3.client("s3",
                             aws_access_key_id=config.aws_access_key_id,
                             aws_secret_access_key=config.aws_secret_access_key,
                             )
      print("Downloading model form s3. {}\n This may take few minutes".format(model_name))
      session.download_file(config.bucket_name,
                            os.path.join(config.model_bucket, config.model_folder_s3, model_name),
                             os.path.join(ml_model_path, model_name))

      return os.path.exists(os.path.join(ml_model_path, model_name))


   
if __name__ == "__main__":
    download_model_from_s3()