from repositories import Product

class CBMService:
    
    @staticmethod
    def getCBM(title):
        product = Product(title)
        pbm_output = product.predict(title)
        return pbm_output