import config
from service import CBMService
from MessageBroker import MessageBrokerFactory
import json
import datetime
import threading
import copy
import os 
import time
import re
import pika

class Task():
    host = config.QUEUE_HOST
    userName = config.QUEUE_USER_NAME
    password = config.QUEUE_PASSWORD
    port = config.QUEUE_PORT

    def __init__(self):
        pass


    def publish(self, data, queue, routeKey, exchangeName, exchangeType):
        Broker = self.createBroker(queue, exchangeName, exchangeType)
        Broker.queue_bind(queue, exchangeName, routeKey)
        Broker.publish(exchangeName, routeKey , data)
        Broker.close_connection()
    
    def subscribe(self):
        queue = config.SUBSCRIBE_QUEUE_NAME
        exchangeName = config.SUBSCRIBE_QUEUE_EXCHANGE_NAME
        exchangeType = config.SUBSCRIBE_QUEUE_EXCHANGE_TYPE
        exchangeKey = config.SUBSCRIBE_ROUTE_KEY
        Broker = self.createBroker(queue, exchangeName, exchangeType)
        Broker.queue_bind(queue, exchangeName,exchangeKey)     
        Broker.subscribe(exchangeName, queue, self.callback)

    def createBroker(self, queue, exchange, exchangeType):
        subscribeBroker = MessageBrokerFactory()
        Broker = subscribeBroker.get_broker('rabbitmq')
        connection = Broker.build_broker(self.host, self.port, self.userName, self.password)
        Broker.open_connection(connection)
        Broker.declare_queue(queue)
        Broker.declare_exchange(exchange,exchangeType)
        return Broker

    def callback(self,channel, method, properties, body):
        app = config.APP_NAME
        try:
            data = json.loads(body)
            # this solves the issue of pikka time out when the process takes more than 180 sec
            thread = threading.Thread(target=self.process_data, args=(data,))
            thread.start()
            while thread.is_alive():  # Loop while the thread is processing
                channel._connection.sleep(1.0)
            channel.basic_ack(delivery_tag=method.delivery_tag)      
        except Exception as e:
            print(e)
            data = json.loads(body)
            config.logging.error('{0} exception for ID {1} : {2}'.format(app, data, e))
            errorData = self.process_error(data, str(e))
            self.publish(errorData,config.ERROR_QUEUE_NAME, config.ERROR_ROUTE_KEY, config.ERROR_QUEUE_EXCHANGE_NAME, config.ERROR_QUEUE_EXCHANGE_TYPE)
            

    def process_output(self, output, data):
        if output is not None:      
            self.publish(data,config.PUBLISH_QUEUE_NAME, config.PUBLISH_ROUTE_KEY, config.PUBLISH_QUEUE_EXCHANGE_NAME, config.PUBLISH_QUEUE_EXCHANGE_TYPE)
        else:
            error = "error in output"
            config.logging.error('{0} exception for ID {1} : {2}'.format(config.APP_NAME, data, error))
            errorData = self.process_error(data, error)
            self.publish(errorData,config.ERROR_QUEUE_NAME, config.ERROR_ROUTE_KEY, config.ERROR_QUEUE_EXCHANGE_NAME, config.ERROR_QUEUE_EXCHANGE_TYPE)


    def process_data(self, data):
        try:
            import time
            start_time = time.time()
            title = data['title']
            print("process data")
            output = CBMService.getCBM(title)
            data["b"]=output["Brand"]
            data["m"]=output["Model"]
            data["p"]=output["Product"]
            jdata = json.dumps(data)
            print(jdata)
            self.process_output(output, jdata)
            print('published ')
        except Exception as e:
            print(e)
            config.logging.error('{0} exception for ID {1} : {2}'.format(config.APP_NAME, data, e))
            errorData = self.process_error(data, str(e))
            self.publish(errorData,config.ERROR_QUEUE_NAME, config.ERROR_ROUTE_KEY, config.ERROR_QUEUE_EXCHANGE_NAME, config.ERROR_QUEUE_EXCHANGE_TYPE)

    
    def process_error(self, data, error):
        currentDT = datetime.datetime.now()
        data['errorTime'] = currentDT.strftime("%Y-%m-%d %H:%M:%S")
        data['api'] = config.APP_NAME
        data['error'] = error
        return json.dumps(data)

    def test(self):
        data = {"description": "Apple iPhone X review", "raw_page_url": "https://dev.yupl.us/infibeam/vu_smart_led_tv.html", 
        "title": "Apple iPhone X", "site_id": "5d3eb63a8b5bb9049e071cdf", "page_url": "https://dev.yupl.us/infibeam/vu_smart_led_tv.html", 
        "site_type": "eCommerce", "is_ecom": "true", "product": ["mobile", "power_banks"], "product_identifier_analytics": 0.0028808116912841797}
        title = data['title']
        # title = "iVooMiMe1"
        output = CBMService.getCBM(title) 
        # print(output)
    

if __name__ == '__main__':
    task = Task()
    print("running task")
    # task.test()
    try:
        task.subscribe()
    except pika.exceptions.StreamLostError:
        print("stream excepton")
        time.sleep(2*60)
        task.subscribe()

